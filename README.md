# 706mm

Icom IC-706 Memory Manager. A fork from https://arachnoid.com/IcomProgrammer/index.html 

What it does: Program the IC-706 MKIIG memories from a computer

Requirements: 
1. Linux computer (probably could work on Mac with a few minor changes)
2. CI-V to USB adapter
3. Libre Office to make spreadshets
4. Python 3
5. pyserial (apt-get install python3-serial)

Read this before starting: 

You need to create an ODS file (Libre Office spreadsheet) with the following columns: 

RxFreq	TxFreq	RxTone	TxTone	Mode	DUP	SPL	TONE	TSQL

Note: You may define other column names if you wish, such as names or callsigns, but they will be ignored by this python script. The column names may appear in any order. Channel numbers are notably absent; this script simply programs the first encountered data into the first channel, and so on. Perhaps future version will change this behavior. 

Each channel needs, at minimum, to contain RxFreq and Mode. (Mode is FM, LSB, USB, AM, CW). RF frequencies are in MHz. The RxFreq will be used as the receive frequency. If a duplex is defined, the radio will handle determining the transmit frequency. For simplex frequencies, just program the RxFreq, and set DUP and SPL to "FALSE". 

For split frequencies, program in the TxFreq, RxTone and TxTone, and place "TRUE" under the SPL column (SPL = split). Blank tones indicate that no PL or Tone Squelch is to be used. 

For repeaters, the DUP column should contain "+" or "-". Define "TxTone" as the tone needed to be transmitted. Use RxTone as the tone for the tone squelch on your radio. If you need different tones for each one, then you will need to program that channel as split. Next, write "TRUE" under either the TONE column (for transmitted tones), or under TSQL (for tone squelch on your receiver). Again, with TSQL, the rig will also transmit the same tone. 

The program should first be run once, and then "Quit" selected from the menu. This creates the empty folders for you within the directory of the script. 

Next, place a file called "706_chans.ods" inside the "frequency_data_ods" directory created by the initial script run. This is the ODS file that you fill with your frequencies. 

The script is designed to use the ttyUSB0 serial port at a baud rate of 19200, with the IC-706 MKIIG using the default CI-V address of 0x58. You can edit the script if you need to use different settings. Don't forget to run ```sudo chown $USER /dev/ttyUSB*``` first. You can check the baud rate of the rig by holding down the LOCK button while you power the radio on. Turn the M.Ch knob until you see the CI-V settings. 

When this is all set up, run the program and select from the menu to program your radio. Make sure the radio is on and the CI-V adapter is connected. 

The program will write to the radio, and it will prompt the user to enter the TONE and TSQL settings on the radio. This is a limitation of the 706 series, there is no way to set the tones over the CI-V interface (the tone can be enabled, but not set). It's not as bad as it sounds, the script will tell the user what buttons to press and it's the same ones over and over again. 

TODO:
1. Support channel numbers in the ODS spreadsheet
2. Program channels in sorted PL tone groups, so that fewer manual changes (by the user) are needed
3. Clean out unused code
4. Clean up print messages and other leftovers from developemnt 
5. Maintain support for other radio models that the code originally supported
